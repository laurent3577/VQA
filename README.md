This project is a keras implementation of the model proposed in https://arxiv.org/pdf/1505.00468.pdf for Visual Question Answering

Some additionnal potential improvements also have been explored, in particular changing the question embedding and modeling attention maps.