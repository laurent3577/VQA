# coding: utf-8

from VQA_API.PythonHelperTools.vqaTools.vqa import VQA
from collections import defaultdict, Counter
import csv
import numpy as np
import pandas as pd
import json, codecs
import sys
from utils import read_json, save_json
from itertools import groupby
from operator import itemgetter

dataDir = '/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/VQA_API'
#dataDir = '/home/ubuntu/VQA/VQA_API'
versionType = 'v2_'  # this should be '' when using VQA v2.0 dataset
taskType = 'OpenEnded'  # 'OpenEnded' only for v2.0. 'OpenEnded' or 'MultipleChoice' for v1.0
dataType = 'mscoco'  # 'mscoco' only for v1.0. 'mscoco' for real and 'abstract_v002' for abstract for v1.0.
dataSubType = 'train2014'
annFile = '%s/Annotations/%s%s_%s_annotations.json' % (dataDir, versionType, dataType, dataSubType)
quesFile = '%s/Questions/%s%s_%s_%s_questions.json' % (dataDir, versionType, taskType, dataType, dataSubType)

def top_answers_per_type_to_json():
    question_types = []
    with open(dataDir + "/QuestionTypes/mscoco_question_types.txt", "rb") as file:
        reader = csv.reader(file)
        for row in reader:
            question_types.append(row[0])

    answer_per_type = defaultdict(list)

    for quest_type in question_types:
        annIds = vqa.getQuesIds(quesTypes=quest_type)
        anns = vqa.loadQA(annIds)
        for ann in anns:
            answer_per_type[quest_type] += [answ['answer'] for answ in ann['answers']]
    top_answers = []
    for quest_type in question_types:
        answers = answer_per_type[quest_type]
        answers_count = Counter(answers)
        best_answer = max(answers_count, key=lambda x:answers_count[x])
        top_answers.append(best_answer)
    q_type_top_answer = dict(zip(question_types, top_answers))
    save_json("top_answer_per_type.json",q_type_top_answer)
    return

def Top_1K_answers_to_json():
    print "Loading annotations file...."
    with open(annFile, "rb") as file:
        annotations_data = json.load(file)
    annotations = annotations_data['annotations']
    print "Annotations loaded"
    all_answers = []
    i = 1
    for annotation in annotations:
        all_answers += [a['answer'] for a in annotation['answers']]
        text = "\rAnnotations computed : {}% done".format(round(float(i) / len(annotations) * 100, 2))
        i += 1
        sys.stdout.write(text)
        sys.stdout.flush()
    print "\nUnique answers loaded"
    answers_count = Counter(all_answers)
    unique_answers = answers_count.keys()
    unique_answers.sort(key=lambda x: answers_count[x], reverse=True)
    top_1K_answers = unique_answers[:1000]
    save_json("top_1K_answers.json", top_1K_answers)
    return

if __name__ == "__main__":

    # initialize VQA api for QA annotations
    vqa = VQA(annFile, quesFile)
    top_answers_per_type_to_json()
    print "Top answers per type generated"
    Top_1K_answers_to_json()
    print "All unique answers generated"
