import json
import numpy as np
import os
import pickle
import sys
import spacy
from utils import read_json, save_json, path_to_id
from ImageEmbedding import image_encoder
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical


class word_encoder():
    def __init__(self):
        self.model = Tokenizer()

    def fit_to_words(self, words):
        self.model.fit_on_texts(words)
        self.voc_size = np.max(self.model.word_index.values())

    def encode_words(self, questions):
        return self.model.texts_to_sequences(questions)

class word_encoder_glove():
    def __init__(self):
        self.model = spacy.load('en', vectors='en_glove_cc_300_1m_vectors')

    def fit_to_words(self, words):
        pass

    def encode_words(self, questions):
        encoded_questions = []
        for question in questions:
            question_tensor = np.zeros((26, 384))
            tokens = self.model(question)
            for j in xrange(len(tokens)):
                question_tensor[j,:] = tokens[j].vector
            encoded_questions.append(question_tensor)
        return np.array(encoded_questions)

def index_image_question_answers(image_dir_path, questions_file_path, annotations_file_path=None):
    if annotations_file_path:
        annotations = read_json(annotations_file_path)["annotations"]
    questions = read_json(questions_file_path)["questions"]

    images_id_to_file_path = {}
    for im_name in os.listdir(image_dir_path):
        id = path_to_id(im_name)
        images_id_to_file_path[id] = image_dir_path + im_name
    data = []
    filtered_out_questions = 0
    top_1K_answers = read_json("top_1K_answers.json")
    for i in range(len(questions)):
        img_id = questions[i]["image_id"]
        img_fp = images_id_to_file_path[img_id]
        q_id = questions[i]["question_id"]
        question = questions[i]["question"]
        if annotations_file_path:
            answer = annotations[i]["multiple_choice_answer"]
            if answer in top_1K_answers:
                data.append({"img":img_fp, "q_id":q_id, "question":question, "ans":answer})
            else: 
                filtered_out_questions += 1
        else:
            data.append({"img":img_fp, "q_id":q_id, "question":question})

    file_name = "Index/" + questions_file_path.split("_")[-2] + "_image_question_answers.json"
    print "Number filtered out questions : ", filtered_out_questions, "out of ", len(questions)
    save_json(file_name, data)
    return data


def create_features_dataset(image_dir_path, questions_file_path, annotations_file_path=None):
    index_data_file = "Index/" + questions_file_path.split("_")[-2] + "_image_question_answers.json"

    if not os.path.exists(index_data_file):
        image_question_answers = index_image_question_answers(image_dir_path, questions_file_path, annotations_file_path)
    else:
        image_question_answers = read_json(index_data_file)

    file_name = "Datasets/" + questions_file_path.split("_")[-2] + "_dataset.pkl"


    # Question embedding
    question_encoder = word_encoder()
    all_questions = [sample["question"].encode('ascii') for sample in image_question_answers]
    question_encoder.fit_to_words(all_questions)
    encoded_questions = question_encoder.encode_words(all_questions)

    # Answers embedding
    if annotations_file_path:
        top_1K_answers = read_json("top_1K_answers.json")
        answers_index = [top_1K_answers.index(sample["ans"]) for sample in image_question_answers]
        encoded_answers = to_categorical(np.array(answers_index), 1000)

    # Image embedding
    model = image_encoder()

    img_features = []
    question_features = []
    labels = []
    index_init = 0
    index = index_init
    former_image = ""
    for i, sample in enumerate(image_question_answers[index_init:]):
        if sample["img"] !=  former_image:
            img_feature = model.encode_image(sample["img"])
        question_feature = encoded_questions[i+index_init]
        if annotations_file_path:
            label = encoded_answers[i+index_init]
            labels.append(label)

        img_features.append(img_feature)
        question_features.append(question_feature)
        former_image = sample["img"]
        if index%100==0:
            text = "\n{} % done.".format(round(float(index)/len(image_question_answers)*100,2))
            sys.stdout.write(text)
            sys.stdout.flush()
        if index%5000==0 and index!=0:
            print "Saving...."
            dataset = {"image_features":np.array(img_features),
                       "question_features":np.array(question_features),
                       "labels":np.array(labels),
                       "question_encoder":question_encoder,
                       "index":index}
            with open(file_name, "wb") as file:
                pickle.dump(dataset, file, protocol=2)
            print "Saving completed"
        index += 1

    img_features = np.array(img_features)
    question_features = np.array(question_features)
    labels = np.array(labels)
    dataset = {"image_features":img_features, "question_features":question_features, "labels":labels, "question_encoder":question_encoder, "index":index}

    print "Saving final file ..."
    with open(file_name, "wb") as file:
        pickle.dump(dataset, file, protocol=2)
    print "File saved"

    return


if __name__ == "__main__":
    image_dir_path = "VQA_API/Images/mscoco/val2014subset-train/"
    questions_file_path = "VQA_API/Questions/v2_OpenEnded_mscoco_val2014subset-train_questions.json"
    annotations_file_path = "VQA_API/Annotations/v2_mscoco_val2014subset-train_annotations.json"

    # image_dir_path = "VQA_API/Images/mscoco/train2014/"
    # questions_file_path = "VQA_API/Questions/v2_OpenEnded_mscoco_train2014_questions.json"
    # annotations_file_path = "VQA_API/Annotations/v2_mscoco_train2014_annotations.json"

    # image_dir_path = "VQA_API/Images/mscoco/test2015/"
    # questions_file_path = "VQA_API/Questions/v2_OpenEnded_mscoco_test2015_questions.json"
    index_image_question_answers(image_dir_path, questions_file_path, annotations_file_path)
    # create_features_dataset(image_dir_path, questions_file_path, annotations_file_path)
    

