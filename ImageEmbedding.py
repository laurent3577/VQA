import cv2
import h5py
import os
import numpy as np
from keras.applications.vgg16 import VGG16


def resize_img(img_file):
    im = cv2.resize(cv2.imread(img_file), (224, 224))

    # The mean pixel values are taken from the VGG authors, which are the values computed from the training dataset.
    mean_pixel = [103.939, 116.779, 123.68]

    im = im.astype(np.float32, copy=False)
    for c in range(3):
        im[:, :, c] = im[:, :, c] - mean_pixel[c]
    
    im = np.expand_dims(im, axis=0) 
    return im


def pop(model):
    '''Removes a layer instance on top of the layer stack.
    This code is thanks to @joelthchao https://github.com/fchollet/keras/issues/2371#issuecomment-211734276
    '''
    if not model.outputs:
        raise Exception('Sequential model cannot be popped: model is empty.')
    else:
        model.layers.pop()
        if not model.layers:
            model.outputs = []
            model.inbound_nodes = []
            model.outbound_nodes = []
        else:
            model.layers[-1].outbound_nodes = []
            model.outputs = [model.layers[-1].output]
        model.built = False

    return model

def VGG_model():
    VGG_ref = VGG16(include_top=True, weights='imagenet', input_tensor=None, input_shape=(224,224,3), pooling="max", classes=1000)
    VGG_16 = pop(VGG_ref)
    return VGG_16

def L2_normalize(vect):
    vect = np.array(vect, dtype='float32')
    return vect / np.linalg.norm(vect)

class image_encoder():
    def __init__(self, L2_norm=False):
        self.model = VGG_model()
        self.L2_norm = L2_norm

    def encode_image(self, image_path):
        im_resized = resize_img(image_path)
        features = self.model.predict(im_resized)[0]
        if self.L2_norm:
            features = L2_normalize(features)
        return features



if __name__ == "__main__":
    image_dir_path = "VQA_API/Images/mscoco/val2014subset/"
    images_files_path = [image_dir_path + im_name for im_name in os.listdir(image_dir_path)]
    im = images_files_path[0]
    im_encoder = image_encoder(L2_norm=False)
    im_resized = resize_img(im)
    print im_resized.shape
    im_rep = im_encoder.encode_image(im)
    print im_rep.shape
    print np.sum(im_rep**2)

