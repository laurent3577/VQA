import numpy as np 
import json
import codecs
import os


def read_json(file_path):
    with open(file_path, "rb") as file:
        data = json.load(file)
    return data

def save_json(file_path, obj):
    with open(file_path, "wb") as file:
        json.dump(obj, codecs.getwriter('utf-8')(file), ensure_ascii=False)
    return

def path_to_id(image_file_path):
    str_id = image_file_path.split("_")[-1].split(".")[0]
    return int(str_id)

def id_to_path(image_id):
	image_id = str(image_id)
	while len(image_id) < 12:
		image_id = "0" + image_id
	return image_id

