import pandas as pd
import csv
import sys
import json
import pickle
from numpy import random
from utils import read_json
from VQA_API.PythonHelperTools.vqaTools.vqa import VQA
from VQA_API.PythonEvaluationTools.vqaEvaluation.vqaEval import VQAEval


class Baseline(object):
    def __init__(self, method):
        self.data_dir = "/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/VQA_API/"
        self.version_type = "v2_"
        self.method = method

    def get_answers(self, questions):
        answers = []
        print "Method for generating answers :", self.method
        i = 1
        if self.method == "Q_prior":
            question_types = self.get_question_type(questions)
            top_answer_per_type = read_json("top_answer_per_type.json")
            for question_type in question_types:
                answer = top_answer_per_type[question_type]
                answers.append(answer)
                text = "\rGenerating answers : {}% done".format(round(float(i)/len(question_types)*100,2))
                i+=1
                if i%100==0:
                    print answer
                    sys.stdout.write(text)
                    sys.stdout.flush()
        elif self.method == "Random":
            with open("top_1K_answers.json", "rb") as answers_file:
                top_1K_answers = json.load(answers_file)
            for question in questions:
                answers.append(top_1K_answers[random.randint(len(top_1K_answers))])
                text = "\rGenerating answers : {}% done".format(round(float(i)/len(questions)*100,2))
                i+=1
                if i%100==0:
                    sys.stdout.write(text)
                    sys.stdout.flush()
        return answers

    def get_question_type(self, questions):
        type_question_path = self.data_dir + "QuestionTypes/mscoco_question_types.txt"
        question_types = []
        questions_types = []
        with open(type_question_path, "rb") as file:
            reader = csv.reader(file)
            for row in reader:
                question_types.append(row[0])
        for question in questions:
            valid_types = [type for type in question_types if type in question]
            valid_types.sort(key=lambda x: len(x), reverse=True)
            if len(valid_types) == 0:
                questions_types.append('none of the above')
            else:
                questions_types.append(valid_types[0])
        return questions_types

    def save_results(self, question_file):
        questions_data = json.load(open(question_file, 'r'))
        task_type = questions_data["task_type"]
        data_type = questions_data["data_type"]
        data_subtype = questions_data["data_subtype"]
        questions_data = questions_data["questions"]
        question_ids = [question["question_id"] for question in questions_data]
        questions = [str(question["question"]).lower() for question in questions_data]
        answers = self.get_answers(questions)
        result_type = self.method

        results = [{"answer": answer, "question_id": question_id} for answer, question_id in zip(answers, question_ids)]
        res_file = '%s/Results/%s%s_%s_%s_%s_results.json' % (self.data_dir, self.version_type, task_type, data_type, data_subtype, result_type)
        with open(res_file, 'w') as outfile:
            json.dump(results, outfile)
        return res_file

    def evaluate(self, question_file, ann_file, res_file=None):
        if not res_file:
            res_file = self.save_results(question_file)
        vqa = VQA(annotation_file=ann_file, question_file=question_file)
        vqaRes = vqa.loadRes(res_file, question_file)
        vqaEval = VQAEval(vqa, vqaRes)
        vqaEval.evaluate()

        # print accuracies
        print "\n"
        print "Overall Accuracy is: %.02f\n" % (vqaEval.accuracy['overall'])
        print "Per Question Type Accuracy is the following:"
        for quesType in vqaEval.accuracy['perQuestionType']:
            print "%s : %.02f" % (quesType, vqaEval.accuracy['perQuestionType'][quesType])
        print "\n"
        print "Per Answer Type Accuracy is the following:"
        for ansType in vqaEval.accuracy['perAnswerType']:
            print "%s : %.02f" % (ansType, vqaEval.accuracy['perAnswerType'][ansType])
        print "\n"

if __name__ == "__main__":
    dataDir = '/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/VQA_API'
    versionType = 'v2_'
    taskType = 'OpenEnded'
    dataType = 'mscoco'
    dataSubType = 'test2015'
    question_file = '%s/Questions/%s%s_%s_%s_questions.json' % (dataDir, versionType, taskType, dataType, dataSubType)
    #ann_file = '%s/Annotations/%s%s_%s_annotations.json' % (dataDir, versionType, dataType, dataSubType)

    Q_prior_answer = Baseline("Q_prior")
    Q_prior_answer.save_results(question_file)
    #Random_answer = Baseline("Random")
    #Random_answer.save_results(question_file)
    
