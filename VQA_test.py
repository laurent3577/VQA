import os, sys
import argparse
from VQA_Model import VQA_model, get_word_encoder



if __name__ == "__main__":
	 # Choose params for execution
    parser = argparse.ArgumentParser()
    parser.add_argument('-image_path', type=str, help='Path of image')
    parser.add_argument('-question', type=str, help='Question to answer')
    parser.add_argument('-trained_on', type=str, default='train2014', help='Choose training dataset to use to define the vocabulary : val2014subset or train2014')
    parser.add_argument('-glove_encoding', type=bool, default=False, help='Use glove encoding for words embedding')
    parser.add_argument('-L2_norm', type=bool, default=False, help='L2 normalize images features')
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(0)
    index_path = "Index/{}_image_question_answers.json".format(args.trained_on +"-train"*(args.trained_on == 'val2014subset'))
    words_encoder = get_word_encoder(index_path)
    L2_norm = args.L2_norm
    glove_encoding = args.glove_encoding
    weights_file = "Weights/vqa_weights_" + args.trained_on + "_glove"*glove_encoding + "_L2_norm"*L2_norm + ".data"
    image_path = args.image_path
    question = args.question
    vqa = VQA_model(words_encoder, L2_norm=L2_norm, weights_file=weights_file, glove_encoding= glove_encoding)
    vqa.full_answer(image_path,question)
