# coding: utf-8

from vqaTools.vqa import VQA
from collections import defaultdict
import csv
import numpy as np
import pandas as pd


dataDir		='/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/VQA_API'
versionType ='v2_' # this should be '' when using VQA v2.0 dataset
taskType    ='OpenEnded' # 'OpenEnded' only for v2.0. 'OpenEnded' or 'MultipleChoice' for v1.0
dataType    ='mscoco'  # 'mscoco' only for v1.0. 'mscoco' for real and 'abstract_v002' for abstract for v1.0.
dataSubType ='train2014'
annFile     ='%s/Annotations/%s%s_%s_annotations.json'%(dataDir, versionType, dataType, dataSubType)
quesFile    ='%s/Questions/%s%s_%s_%s_questions.json'%(dataDir, versionType, taskType, dataType, dataSubType)
# initialize VQA api for QA annotations
vqa=VQA(annFile, quesFile)



"""
All possible quesTypes for abstract and mscoco has been provided in respective text files in ../QuestionTypes/ folder.
"""
question_types = []
with open(dataDir + "/QuestionTypes/mscoco_question_types.txt", "rb") as file:
    reader = csv.reader(file)
    for row in reader:
        question_types.append(row[0])

answer_per_type = defaultdict(list)

for quest_type in question_types:
    annIds = vqa.getQuesIds(quesTypes=quest_type)
    anns = vqa.loadQA(annIds)
    for ann in anns:
        answ = ann['answers'][0]
        answer_per_type[quest_type].append(answ['answer'])
top_answers = []
for quest_type in question_types:
    answers = answer_per_type[quest_type]
    unique_answers = list(set(answers))
    best_answer = unique_answers[0]
    best_count = answers.count(best_answer)
    for ans in unique_answers[1:]:
        count = answers.count(ans)
        if count > best_count:
            best_answer = ans
            best_count = count
    top_answers.append(best_answer)

data = np.array([question_types, top_answers])
data = data.transpose()
df = pd.DataFrame(data=data, columns=["Question Type", "Top Answer"])
df.to_csv("/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/top_answer_per_type.csv")
