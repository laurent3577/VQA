from keras.models import Model
from keras.layers import Input, Dense, Dropout, LSTM, Embedding, Merge, GRU, Lambda
from keras.callbacks import ModelCheckpoint
from keras.layers.merge import Multiply, Concatenate
from keras.preprocessing.sequence import pad_sequences
from ImageEmbedding import resize_img, image_encoder, L2_normalize
from preprocess import word_encoder, word_encoder_glove
from VQA_API.PythonHelperTools.vqaTools.vqa import VQA
from VQA_API.PythonEvaluationTools.vqaEvaluation.vqaEval import VQAEval
from keras import optimizers
from utils import read_json, save_json, id_to_path
import pickle
import numpy as np
import os, sys
import argparse


class VQA_model():
    def __init__(self, word_encoder, L2_norm=False, weights_file=None, glove_encoding= False, show_summary=False):

        ######################### Setting Storing parameters

        self.data_dir = "/Users/Laurent/Documents/Centrale/3A/MVA/ObjectRecognition/VQA/VQA_API/"
        #self.data_dir = '/home/ubuntu/VQA/VQA_API/'

        self.version_type = "v2_"

        #############################################################


        ######################### Setting model parameters

        self.max_quest_length = 26
        self.possible_answers = read_json("top_1K_answers.json")
        self.glove_encoding = glove_encoding
        self.L2_norm = L2_norm

        #############################################################


        ######################### Setting Encoders

        # Model to encode images
        self.image_encoder = image_encoder(self.L2_norm)

        # Model to encode questions
        self.word_encoder = word_encoder

        #############################################################


        ######################### Setting Keras Model
        
        dropout_rate = 0.5

        ######################### Text model

        if not glove_encoding:
            voc_size = self.word_encoder.voc_size
            question_input = Input(shape=(self.max_quest_length,), dtype='float32')
            embedded_question = Embedding(input_dim=voc_size+1, output_dim=300, input_length=self.max_quest_length, mask_zero=True)(question_input)
            encoded_question = LSTM(units=512, return_sequences=True, return_state=True)(embedded_question)
            #encoded_question = GRU(units=512, return_sequences=True, return_state=True)(embedded_question)
            #first_lstm_output = Lambda(lambda x: x[:,-1,:])(encoded_question[0])
        else:
            question_input = Input(shape=(self.max_quest_length,384), dtype='float32')
            encoded_question = LSTM(units=512, return_sequences=True, return_state=True)(question_input)
            #encoded_question = GRU(units=512, return_sequences=True, return_state=True)(question_input)
            #first_lstm_output = Lambda(lambda x: x[:,-1,:])(encoded_question[0])
            
        drop_out = Dropout(dropout_rate)(encoded_question[0])
        encoded_question_2 = LSTM(units=512, return_sequences=False, return_state=True)(drop_out)
        #encoded_question_2 = GRU(units=512, return_sequences=False, return_state=True)(drop_out)
        final_lstm_output = Concatenate()([encoded_question[1], encoded_question[2], encoded_question_2[1], encoded_question_2[2]])
        #final_lstm_output = Concatenate()([first_lstm_output, encoded_question[1], encoded_question_2[0], encoded_question_2[1]])
        drop_out_2 = Dropout(dropout_rate)(final_lstm_output)
        text_encoding = Dense(1024, activation='tanh')(drop_out_2)

        ######################### Image model

        image_input = Input(shape=(4096,))
        # attention_input = Concatenate()([image_input, final_lstm_output])
        # attention_probs = Dense(4096, activation='softmax', name='attention_probs')(attention_input)
        # image_input_attention = Multiply()([image_input, attention_probs])
        # image_encoding = Dense(1024, input_dim=4096, activation='tanh')(image_input_attention)
        image_encoding = Dense(1024, input_dim=4096, activation='tanh')(image_input)


        ######################### Merging question and images

        merged = Multiply()([text_encoding, image_encoding])
        last_hidden = Dense(1000, activation='tanh')(merged)
        output = Dense(1000, activation='softmax')(last_hidden)
        self.vqa_model = Model(inputs=[image_input, question_input], outputs=output)
        self.vqa_model.compile(optimizer='rmsprop', loss='categorical_crossentropy',  metrics=['accuracy'])

        #############################################################

        ######################### Setting weights if given

        if weights_file:
            print "Loading Weights..."
            self.vqa_model.load_weights(weights_file)
            print "Weights loaded."

        #############################################################
        if show_summary:
            print " ##################      Model Summary     #################"
            self.vqa_model.summary()

    def train(self, X, y, task, epochs=15, batch_size=64, save_weights=True):
        if save_weights:
            weights_file = "Weights/vqa_weights_" + task + "_glove"*self.glove_encoding + "_L2_norm"*self.L2_norm + ".data"
            checkpointer = ModelCheckpoint(filepath=weights_file, monitor='acc', verbose=1, save_best_only=True)
            hist = self.vqa_model.fit(X, y, epochs=epochs, batch_size=batch_size, verbose=1, callbacks=[checkpointer])
            self.vqa_model.load_weights(weights_file)
        else:
            self.vqa_model.fit(X, y, epochs=epochs, batch_size=batch_size, verbose=1)

    def predict(self, X):
        answer =  self.vqa_model.predict(X)
        answer_str = self.possible_answers[np.argmax(answer)]
        return answer_str

    def answer(self, img_path, question_str):
        img_input = self.image_encoder.encode_image(img_path)
        question_encoded = self.word_encoder.encode_words([question_str])
        question_input = pad_sequences(question_encoded, maxlen= self.max_quest_length, padding='pre')
        answer = self.predict([np.array([img_input]), question_input])
        return answer

    def full_answer(self, img_path, question_str):
        img_input = self.image_encoder.encode_image(img_path)
        question_encoded = self.word_encoder.encode_words([question_str])
        question_input = pad_sequences(question_encoded, maxlen= self.max_quest_length, padding='pre')
        answer = self.vqa_model.predict([np.array([img_input]), question_input])[0]
        top_answers = np.argsort(answer)[-5:]
        top_answers_str = [(self.possible_answers[i], answer[i]) for i in top_answers]
        top_answers_str.reverse()
        for ans, conf in top_answers_str:
            print ans, "  conf : ", round(conf, 2), "%"
        return top_answers_str

    def save_results(self, question_file):
        questions_data = read_json(question_file)
        task_type = questions_data["task_type"]
        data_type = questions_data["data_type"]
        data_subtype = questions_data["data_subtype"]
        questions_data = questions_data["questions"]
        result_type = "LSTM_CNN_" + self.L2_norm*"I"

        im_dir_path = self.data_dir + "Images/mscoco/" + data_subtype + "/"
        res_file = '%s/Results/%s%s_%s_%s_%s_results.json' % (self.data_dir, self.version_type, task_type, data_type, data_subtype, result_type)
        index_file = res_file.split(".")
        index_file[0] += "_index"
        index_file = ".".join(index_file)
        if os.path.exists(res_file):
            results = read_json(res_file)
            index_init = read_json(index_file)["index"]
        else:
            results = []
            index_init = 0
        i = 1
        index = index_init
        former_image_id = 123456789123456789
        for question in questions_data[index_init:]:
            question_str = question["question"]
            image_id = question["image_id"]
            question_id = question["question_id"]
            if image_id != former_image_id:
                image_path = im_dir_path + "_".join(["COCO", data_subtype.split("-")[0], id_to_path(image_id)+".jpg"])
                img_input = self.image_encoder.encode_image(image_path)
            question_encoded = self.word_encoder.encode_words([question_str])
            question_input = pad_sequences(question_encoded, maxlen= self.max_quest_length, padding='pre')
            answer = self.predict([np.array([img_input]), question_input])
            
            results.append({"answer": answer, "question_id": question_id})
            former_image_id = image_id
            if i%250==0:
                print "{} % done.".format(round(float(index)/len(questions_data)*100,2))
                save_json(res_file, results)
                save_json(index_file, {"index":index})
            i += 1
            index += 1
        
        save_json(res_file, results)
        return res_file

    def evaluate(self, question_file, ann_file, res_file=None):
        if not res_file:
            res_file = self.save_results(question_file)
        vqa = VQA(annotation_file=ann_file, question_file=question_file)
        vqaRes = vqa.loadRes(res_file, question_file)
        vqaEval = VQAEval(vqa, vqaRes)
        vqaEval.evaluate()

        # Show accuracies
        print "\n"
        print "Overall Accuracy is: %.02f\n" % (vqaEval.accuracy['overall'])
        print "Per Answer Type Accuracy is the following:"
        for ansType in vqaEval.accuracy['perAnswerType']:
            print "%s : %.02f" % (ansType, vqaEval.accuracy['perAnswerType'][ansType])
        print "\n"


# Some utiliritary functions

def load_data(list_file_paths, max_quest_length=26, L2_norm=False, glove_encoding=False):
    image_features = []
    labels = []
    question_features = []
    for dataset_file_path in list_file_paths:
        with open(dataset_file_path, "rb") as file:
            data = pickle.load(file)
        image_features.append(data["image_features"])
        labels.append(data["labels"])
        if not glove_encoding:
            question_features_dataset = data["question_features"]
            question_features_dataset = pad_sequences(question_features_dataset, maxlen=max_quest_length, padding='pre')
            question_features.append(question_features_dataset)
            words_encoder = data["question_encoder"]
    image_features = np.concatenate(image_features)
    if L2_norm:
        image_features = np.array([L2_normalize(img_vect) for img_vect in image_features])
    labels = np.concatenate(labels)
    if not glove_encoding:
        question_features = np.concatenate(question_features)
    if glove_encoding:
        preprocessed_data_file = "Index/"+ dataset_file_path.split("/")[1].split("_")[0] + "_image_question_answers.json"
        image_question_answers = read_json(preprocessed_data_file)
        glove_question_encoder = word_encoder_glove()
        all_questions = [sample["question"] for sample in image_question_answers]
        question_features = glove_question_encoder.encode_words(all_questions)
        words_encoder = glove_question_encoder

    return image_features, question_features, labels, words_encoder

def get_word_encoder(index_path):
    questions = read_json(index_path)
    question_encoder = word_encoder()
    all_questions = [sample["question"].encode('ascii') for sample in questions]
    question_encoder.fit_to_words(all_questions)
    return question_encoder




if __name__ == "__main__":

    # Choose params for execution
    parser = argparse.ArgumentParser()
    parser.add_argument('-training_data', type=str, default='val2014subset', help='Choose training dataset to use : val2014subset or train2014')
    parser.add_argument('-load_training_data', type=bool, default=True, help='Load training data or not')
    parser.add_argument('-evaluate', type=bool, default=True, help='Evaluate on predefined subset')
    parser.add_argument('-show_examples', type=bool, default=True, help='Show examples of QA')
    parser.add_argument('-weights_file', type=str, default='None', help='Weights file path')
    parser.add_argument('-glove_encoding', type=bool, default=False, help='Use glove encoding for words embedding')
    parser.add_argument('-L2_norm', type=bool, default=False, help='L2 normalize images features')
    parser.add_argument('-nb_epochs', type=int, default=10, help='Number of epochs for training')
    parser.add_argument('-batch_size', type=int, default=128, help='Batch size for training')
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(0)

    conf = args.training_data
    load_training_data = args.load_training_data
    evaluate = args.evaluate
    show_examples = args.show_examples

    # Weights to load

    #weights_file = "Weights/vqa_weights_val2014subset.data"
    #weights_file = "Weights/vqa_weights_val2014subset_glove.data"
    #weights_file = "Weights/vqa_weights_train2014.data"
    #weights_file = "Weights/vqa_weights_train2014_L2_norm.data"
    if args.weights_file == 'None':
        weights_file = None
    else:
        weights_file = args.weights_file
    # Features params

    glove_encoding = args.glove_encoding
    L2_norm = args.L2_norm
    max_quest_length = 26

    # Training params

    nb_epochs = args.nb_epochs
    batch_size = args.batch_size

    # Test file

    question_test_file = "VQA_API/Questions/v2_OpenEnded_mscoco_val2014subset-eval_questions.json"
    annotation_test_file = "VQA_API/Annotations/v2_mscoco_val2014subset-eval_annotations.json"

    # Result file (if already computed)

    #result_file = "VQA_API/Results/v2_Open-Ended_mscoco_val2014subset-eval_LSTM_CNN__results.json"
    result_file = None

    # Example file

    example_file = "Index/val2014subset-eval_image_question_answers.json"



    if conf == "val2014subset":    
        dataset_file_path = "Datasets/val2014subset-train_dataset.pkl"
        list_file_paths = [dataset_file_path]
        index_path = "Index/val2014subset-train_image_question_answers.json"

    if conf == "train2014":
        dataset_file_path = "Datasets/train2014_dataset.pkl"
        dataset_file_path_2 = "Datasets/train2014_dataset_2.pkl"
        dataset_file_path_3 = "Datasets/train2014_dataset_3.pkl"
        dataset_file_path_4 = "Datasets/train2014_dataset_4.pkl"
        dataset_file_path_5 = "Datasets/train2014_dataset_5.pkl"
        list_file_paths = [dataset_file_path, dataset_file_path_2, dataset_file_path_3, dataset_file_path_4, dataset_file_path_5]
        index_path = "Index/train2014_image_question_answers.json"


    # Loading data for training and question embedding
    
    print "Loading data......."

    if load_training_data:
        image_features, question_features, labels, words_encoder = load_data(list_file_paths, max_quest_length, L2_norm, glove_encoding)
        X = [image_features, question_features]
        y = labels
        training_data = True
    else:
        words_encoder = get_word_encoder(index_path)
        training_data = False
    print "Data loaded"
    

    print "Loading VQA model...."
    vqa = VQA_model(words_encoder, L2_norm=L2_norm, weights_file=weights_file, glove_encoding= glove_encoding, show_summary=True)
    print "Model loaded"
    if training_data:
        print "Training of model...."
        vqa.train(X, y, conf, nb_epochs, batch_size)
        print "Model trained"
        print "Recap of training :"
        print "\n Configuration : ", conf
        print " Glove encoding : ", glove_encoding
        print " L2 normalisation of image features : ", L2_norm, "\n"
    if evaluate:
        vqa.evaluate(question_test_file, annotation_test_file, result_file)
    if show_examples:
        data = read_json(example_file)
        for sample in data[:30]:
            print sample["question"]
            print "Model answer", vqa.answer(sample["img"], sample["question"]),  "  / ", "True ans", sample["ans"]
            print ""



